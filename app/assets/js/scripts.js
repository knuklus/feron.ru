;
"use strict";
function DOMready() {

	function sliderRange(data) {
		var from = data.from,
			to = data.to,
			$slider = $(data.slider).siblings('.range-slider__input-wrapper');
		$slider.find('.js--range-from').val(from);
		$slider.find('.js--range-to').val(to);
	}
	$('body').on('input keyup', '.js--range-to, .js--range-from', function (e) {
		var slider = $(this).parent().siblings(".js--range-slider").data("ionRangeSlider");
		slider.update({
			from: $(this).parent().find('.js--range-from').val(),
			to: $(this).parent().find('.js--range-to').val(),
		});
	});

	$(".js--range-slider").ionRangeSlider({
		type: "double",
		min: $(this).data('min'),
		max: $(this).data('max'),
		to: $(this).data('to'),
		from: $(this).data('from'),
		hide_min_max: true,
		hide_from_to: true,
		onStart: function (data) {
			sliderRange(data);
		},
		onChange: function (data) {
			sliderRange(data);
		},
		onFinish: function (data){
			$slider = $(data.slider).siblings('.range-slider__input-wrapper');
			$slider.find('.js--range-from').keyup();
		}
	});

/*
	$(".js--anchor").on("click", function (e) {
		e.preventDefault();
		var anchor = $(this),
			target = anchor.data('target'),
			targetPos;

		if (target == 'top') {
			targetPos = 0
		} else {
			targetPos = $(target).offset().top
		}
		// временный костыль
		$(window).scrollTop(0);

		// ранее рабочий код
		//$(window).animate({
		//	scrollTop: targetPos
		//}, 1500);
		
		
		return false;
	});*/

/*
	// функционал скрытия/показа кнопки прокрутки вверх
	function upBtnVisible() {
		var posTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
		if (posTop > 2000) {
			$('.up-button').addClass('up-button_visible')
		} else {
			$('.up-button').removeClass('up-button_visible')
		}
	}
	$(window).on('scroll', upBtnVisible);
	upBtnVisible();*/

	
/*
	// открытие закрытие показать еще-скрыть(хидденер)
	$('body').on('click', '.js--toggle-hiddener', function (e) {
		e.stopPropagation();
		e.preventDefault();
		$(this).siblings('.hiddener__content').toggleClass('hiddener__content_hide');
		$(this).siblings('.hiddener__content').slideToggle();
	});*/


	//dropdown
	$('body').on('click', '.js-dropdown', function (e) {
		e.preventDefault();
		$_this = $(this);
		$_this.parent().toggleClass('dropdown_visible');
		$(document).mouseup(function (e) { // событие клика по веб-документу
			var div = $_this.parent(); // тут указываем ID элемента
			if (!div.is(e.target) // если клик был не по нашему блоку
				&&
				div.has(e.target).length === 0) { // и не по его дочерним элементам
				div.removeClass('dropdown_visible');
			}
		});
	});


	// модальки
/*	$('body').on('click', '.js--open-modal', function (e) {
		e.preventDefault();
		modal = $(this).data('modal');
		$('#' + modal).arcticmodal();
	});*/


	// фиксация высоты карточек
/*	function catalogItemFixHeight() {
		console.log('1');
		$.map($('.js-catalog-item'), function (el) {
			$(el).css('height', 'auto');
		});
		$.map($('.js-catalog-item'), function (el) {
			height = $(el).css('height');
			$(el).css('height', height);
		});
	};
	$(window).on('resize', catalogItemFixHeight);
	setTimeout(catalogItemFixHeight, 1000);*/

}

document.addEventListener("DOMContentLoaded", DOMready);